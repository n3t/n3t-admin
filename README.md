n3t Admin
=========

n3t Admin is system plugin changing some Joomla! administration behaviors, to make
user experience more friendly.

Currently supported mods
------------------------

### Single articles category filter

When enabled, category selection in articles list filter will allow to select only
one category at a time (multiple is default behavior). This enables faster filtering
of articles in specific categories.

### Disable validation on banners click url

When enabled, Click URL field in Banners component is not validated as URl, means
you can fill relative URLs, or any other value.