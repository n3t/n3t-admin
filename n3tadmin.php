<?php
/**
 * @package n3t Admin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2018-2021 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

class plgSystemN3tAdmin extends JPlugin {

  protected $autoloadLanguage = true;


  public function onContentPrepareForm(JForm $form, $data)
  {
    $name = $form->getName();
    $app = JFactory::getApplication();
    $doc = JFactory::getDocument();

    // Single select in articles list
    if (
      ($name == 'com_content.articles.filter' || $name == 'com_content.articles.modal.filter' || $name == 'com_content.featured.filter')
      && $app->isClient('administrator')
      && $this->params->get('articles_filter_category_single', 1)
      && $form->getFieldAttribute('category_id', 'multiple', 'false', 'filter') == 'true'
    ) {
      // set field attributes
      $form->setFieldAttribute('category_id', 'multiple', 'false', 'filter');
      $form->setFieldAttribute('category_id', 'class', '', 'filter');

      // Add empty option
      $field = $form->getFieldXml('category_id', 'filter');
      $fieldDom = dom_import_simplexml($field);
      $hasChildren = $fieldDom->hasChildNodes();
      $option = $field->addChild('option', JText::_('JOPTION_SELECT_CATEGORY'));

      if ($hasChildren) {
        $optionDom = $fieldDom->ownerDocument->importNode(dom_import_simplexml($option), true);
        $fieldDom->insertBefore($optionDom, $fieldDom->firstChild);
      }
      return true;
    }

    // Banners any URL
    if (
      $name == 'com_banners.banner'
      && $app->isClient('administrator')
    ) {
      if ($this->params->get('banners_clickurl_filter_text', 1)) {
        $form->setFieldAttribute('clickurl', 'filter', 'text');
        $form->setFieldAttribute('clickurl', 'validate', '');
      }

      if ($this->params->get('banners_enable_impmade', 0)) {
        $doc->addScriptDeclaration('
          jQuery(function($){$("#jform_impmade").prop("readonly", false);});
        ');
      }

      if ($this->params->get('banners_enable_clicks', 0)) {
        $doc->addScriptDeclaration('
          jQuery(function($){$("#jform_clicks").prop("readonly", false);});
        ');
      }
    }

    return true;
  }
}
