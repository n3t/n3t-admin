n3t Admin
=========

n3t Admin is system plugin changing some Joomla! administration behaviors, to make
user experience more friendly.

Currently supported mods
------------------------

### Single articles category filter

When enabled, category selection in articles list filter will allow to select only
one category at a time (multiple is default behavior). This enables faster filtering
of articles in specific categories.

### Banners URLs as free text

When enabled, URL field in Banners component do not validate entered value as URL anymore.
So you can enter relative URLs, or any other value you wish.

### Edit Banners Impressions and Clicks
When enabled, Total Impressions and Total clicks fields in Banners component are
enbaled, and could be changed.