Usage
=====

Installation
------------

n3t Admin could be installed as any other extension in Joomla! For detailed
information see Joomla documentation.

Do not forget to __enable the plugin__ after installation. 

Joomla! versions
----------------

Currently only Joomla 3.x and newer is supported.

More information could be found in [Release notes](about/release-notes.md)
