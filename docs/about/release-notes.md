Release notes
=============

3.0.x
-----

#### 3.0.6

- enable Banners Total Impresions and Total Clicks fields editing

#### 3.0.5

- added support for featured articles list
- disable banners Click URL validation rule
- improved installer

#### 3.0.4

- added support for articles list in modal dialog

#### 3.0.3

- added help URL
- added support for Banners - no validation for 'Click url'
- corrected default parameters values in plugin code

#### 3.0.2

- solved PHP warning, added missing quotes

#### 3.0.1

- Doubled empty option issue

#### 3.0.0

- Initial release
